Psigama
=========
 |Psigama|

Estudo da mente interaciológica a partir da Psicogênese do Pe. Xavier.

 |docs| |python| |license| |github|


:Author:  Carlo E. T. Oliveira
:Version: 21.02
:Affiliation: Universidade Federal do Rio de Janeiro
:License: GNU General Public License v3 or later (GPLv3+)
:Homepage: `Projeto Psigama`_
:Changelog: `CHANGELOG <CHANGELOG.rst>`_

Psigama
-------

Estudo da mente interaciológica a partir da Psicogênese do Pe. Xavier.

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

LABASE_ - NCE_ - UFRJ_

|LABASE|

.. _LABASE: http://labase.activufrj.nce.ufrj.br
.. _NCE: http://nce.ufrj.br
.. _UFRJ: http://www.ufrj.br

.. _Projeto Psigama: https://activufrj.nce.ufrj.br/wiki/labase/PsiGama_Psicogenetica

.. |github| image:: https://img.shields.io/badge/release-21.02-blue
   :target: https://gitlab.com/labasence/psigama/-/releases


.. |LABASE| image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :target: http://labase.activufrj.nce.ufrj.br
   :alt: LABASE

.. |Psigama| image:: https://imgur.com/7ycdK6P.png
   :target: https://gitlab.com/labasence/psigama
   :alt: PSIGAMA
   :width: 800px

.. |python| image:: https://img.shields.io/badge/language-python-blue
   :target: https://www.python.org/downloads/release/python-383/

.. |docs| image:: https://img.shields.io/badge/docs-none-orange
   :target: https://i.imgur.com/MjJUb0f.jpg

.. |license| image:: https://img.shields.io/badge/licence-GPL--3.0-orange
   :target: https://gitlab.com/labasence/psigama/-/raw/master/LICENSE
