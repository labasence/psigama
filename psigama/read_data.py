#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program Meggido
# Copyright © 2021  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Load and clean data from Raw txt files.

    This module reads instrumented data collected from Meggido game.

Classes in this module:

    :py:class:`Loader` loads data from files.

Changelog
---------
    21.01
        * NEW: Initial Implementation.

.. seealso::

   Page :ref:`core_introduction`

"""


class Loader:
    DATA = "../../../../data/experimento/"
    GRUPO = "DADOS-COLETADOS PESQUISA-ESCOLAS TAQUARA RECREIO".split()
    TAQUARA = "GRUPO1-TRIBAL GRUPO3-TRIBAL GRUPO5-SELETIVO" \
              " GRUPO2-TRIBAL GRUPO4-SELETIVO  GRUPO6-SELETIVO"
    RECREIO = "GRUPO10-SELETIVO GRUPO1-TRIBAL GRUPO4-TRIBAL GRUPO7-SELETIVO " \
              "GRUPO11-SELETIVO GRUPO2-TRIBAL GRUPO5-TRIBAL GRUPO8-SELETIVO " \
              "GRUPO12-SELETIVO  GRUPO3-TRIBAL  GRUPO6-TRIBAL  GRUPO9-SELETIVO"

    def __init__(self):
        self.all_data = {}

    def loads(self):
        """Loads from File"""
        from collections import Counter
        import re
        with open("{}/{}/GRUPO_01.txt".format(self.DATA, self.GRUPO[0]), "r") as g1:
            self.all_data = lines = g1.readlines()
            linelist = [tuple(re.findall("[a-z]+", line)) for line in lines
                        if len(line.split()) > 3]
            eves = {" ".join(key): value for key, value
                    in Counter(linelist).items()}
            [print("{}: {}".format(*its)) for its in eves.items()]

    def recover(self):
        """Recover data from File"""
        from collections import Counter
        import re
        from csv import writer

        def acr(line):
            name = re.findall("[a-z]+", line)
            return "{}{}{}".format(
                name[0][0], name[1][0], name[2][0] if len(name) > 2 else name[1][1]) if name else ""
        with open("{}/{}/GRUPO_01.txt".format(self.DATA, self.GRUPO[0]), "r") as g1:
            self.all_data = lines = g1.readlines()
            linedic = [tuple([ags[0].split(".")[-1][:-2], acr(line), float(ags[-1])*100])
                       for line in lines if len(ags := line.split()) > 3]
            linedic = [item for item in linedic if item[1]]
            timemin = float(min(float(item[-1]) for item in linedic))
            linedic = [item[:-1] + tuple([int(item[-1]-timemin)]) for item in linedic if item[1]]
            eves = {key: value for key, value
                    in Counter(tuple(ip[0:2]) for ip in linedic).items()}
            [print("{}: {}".format(*its)) for its in eves.items()]
            with open("ipnb/grupo.csv", "w") as gp1:
                linedic = [tuple("user act time".split())] + linedic
                writer(gp1).writerows(linedic)


if __name__ == "__main__":
    Loader().recover()
